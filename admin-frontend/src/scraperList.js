import {
    List,
    Datagrid,
    TextField,
    DateField,
    Pagination,
    ReferenceField,
    ChipField
} from 'react-admin';


import Chip from '@material-ui/core/Chip';
import { makeStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import PostScraperButton from './postScraperButton';

const AsideSportScrape = () => (
    <div style={{ width: 400, margin: '1em' }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>
                        <p>
                            Hier sind alle Sportarten gelistet, die gerade in <a href="https://www.buchsys.de/fu-berlin/angebote/aktueller_zeitraum/index.html">buchsys.de</a> stehen.
                        </p>
                        <p>Diese werden verglichen mit den Sportarten, die momentan in der Sportliste stehen. Hierbei kann es zu vier verschiedenen Fällen kommen:</p>
                        <ul>
                            <li><i style={{ color: 'black', background: 'lightGrey' }}>Gleicher Sport:</i> <br /> Der gefundene Sport steht bereits in der Datenbank und wird bis auf die URL nicht aktualisiert.</li>
                            <li><i style={{ color: 'black', background: '#A5FF7E' }}>Neuer Sport:</i> <br /> Der gefundene Sport existiert in der Datenbank noch nicht und wird vollkommen neu angelegt. Die Kriterien müssen hier noch ausgefüllt werden.</li>
                            <li><i style={{ color: 'black', background: '#FFFB8C' }}>Sport aus Archiv:</i> <br /> Der gefundene Sport existiert zwar, liegt momentan aber in dem Archiv. Der Sport wird wieder aktiviert werden, und die alten Kriterienbewertungen werden von diesem Sport übernommen werden. </li>
                            <li><i style={{ color: 'black', background: '#FF8EAA' }}>Sport ins Archiv:</i> <br /> Es wurde kein neuer Sport gefunden, der dem Sport in der Datenbank zu gleichen scheint, also wird der Sport in das Archiv geschoben, um nicht mehr als Sportempfehlung angezeigt zu werden.</li>
                        </ul>
                    </li>
                    <li>
                        Die Sportarten können mit der Checkbox links ausgewählt werden. Soweit mindestens ein Sport ausgewählt wurde, erscheint der Knopf <b>Ausgewählte Sportarten hinzufügen</b>, um die ausgewählten Änderungen anzuwenden und zu den Incomplete Sports zu springen.
                    </li>
                </ul>
            </CardContent>
        </Card>
    </div>
);




const useStyles = makeStyles({
    same: { color: 'black', background: 'lightGrey' },
    new: { color: 'black', background: '#A5FF7E' },
    from_archive: { color: 'black', background: '#FFFB8C' },
    to_be_archived: { color: 'black', background: '#FF8EAA' },
});

const DiffTypeChip = props => {
    const classes = useStyles();

    let verbose_type = "";
    if (props.record.kind_of_diff === "new") {
        verbose_type = "Neuer Sport";
    } else if (props.record.kind_of_diff === "same") {
        verbose_type = "Gleicher Sport";
    } else if (props.record.kind_of_diff === "from_archive") {
        verbose_type = "Sport aus Archiv";
    } else {
        verbose_type = "Sport ins Archiv";
    };

    return (
        <Chip label={verbose_type} className={classes[props.record.kind_of_diff]} />
    );
};






const ScraperPagination = props => <Pagination rowsPerPageOptions={[]} {...props} />;

/* bulkActionButtons should theoretically be able to take a whole Fragment worth of buttons, but React hates us and we return the feeling*/

// perPage is set to 1000
// It is important that the page shows all entries, so that there are no issues while scrolling through the pages and sending the data afterwards
// So, all Diffs should be displayed on the same page
export const ScraperList = props => (
    <List
        {...props}
        perPage={1000}
        pagination={<ScraperPagination />}
        bulkActionButtons={<PostScraperButton {...props} />}
        aside={<AsideSportScrape />}
    >
        <Datagrid rowClick="edit">
            <TextField
                source="id"
                sortable={false}
            />
            <DiffTypeChip
                source="kind_of_diff"
                label="Art der Differenz"
                sortable={false}
            />

            <ReferenceField reference="sport" source="old_sport.id" label="Sport ID">
                <ChipField
                    source="id"
                    label="Alte ID"
                    sortable={false}
                />
            </ReferenceField>

            <DateField
                source="old_sport.last_used"
                label="Zuletzt aktiviert"
                locales="fr-FR"
                sortable={false}
            />
            <TextField
                source="old_sport.name"
                label="Alter Sport"
                sortable={false}
            />
            <TextField
                source="new_sport.name"
                label="Neuer Sport"
                sortable={false}
            />
        </Datagrid>
    </List>
);