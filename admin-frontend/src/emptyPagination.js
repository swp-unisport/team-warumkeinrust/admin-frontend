import { Pagination } from 'react-admin';

// Removes the pagination field at the bottom of the page
// Used whenever flipping pages on a view might crash things
export default (props => <Pagination rowsPerPageOptions={[]} {...props} />)