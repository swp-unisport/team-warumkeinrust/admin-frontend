import * as React from 'react';
import {
    List,
    Datagrid,
    TextField,
    Edit,
    SimpleForm,
    BooleanField,
    TextInput,
    ImageInput,
    ImageField,
    Create,
    required
} from 'react-admin';

import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'

const AsideSnackList = () => (
    <div style={{ width: 400, margin: '1em' }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>Klicke auf eine Datenreihe, um sie zu bearbeiten.</li>
                    <br />
                    <li>Wissenssnacks/Aktivitäten müssen eine deutsche Übersetzung und können eine englische Übersetzung haben.</li>
                    <br />
                </ul>

            </CardContent>
        </Card>
    </div>
);

const AsideSnackEdit = () => (
    <div style={{ width: 400, margin: '1em' }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>Klicke auf eine Datenreihe, um sie zu bearbeiten.</li>
                    <br />
                    <li>Wissenssnacks/Aktivitäten müssen eine deutsche Übersetzung und können eine englische Übersetzung haben.</li>
                    <br />
                    <li>Es wird das Bild angezeigt, was momentan im Quiz benutzt wird. Wenn ein neues Bild hochgeladen wird, so wird das Alte überschrieben.</li>
                    <br />
                </ul>

            </CardContent>
        </Card>
    </div>
);

const AsideSnackCreate = () => (
    <div style={{ width: 400, margin: '1em' }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>Klicke auf eine Datenreihe, um sie zu bearbeiten.</li>
                    <br />
                    <li>Wissenssnacks/Aktivitäten müssen eine deutsche Übersetzung und können eine englische Übersetzung haben.</li>
                    <br />
                    <li>Es kann außerdem das Bild hochgeladen werden, was im Quiz für diesen Snack/diese Aktivität angezeigt werden soll.</li>
                    <br />
                </ul>

            </CardContent>
        </Card>
    </div>
);


export const snackList = props => (
    <List {...props}
        mutationMode={"pessimistic"}
        aside={<AsideSnackList />}
    >
        <Datagrid rowClick="edit">
            <TextField
                source="id"
                label="Id"
                sortable={false}
            />
            <TextField
                source="text_de"
                label="Deutscher Text"
                sortable={false}
            />
            <TextField
                source="text_en"
                label="Englischer Text"
                sortable={false}
            />
            <BooleanField
                source="has_image"
                label="Hat Bild"
                sortable={false}
            />
        </Datagrid>
    </List>
);

export const snackEdit = props => (
    <Edit {...props}
        mutationMode={"pessimistic"}
        aside={<AsideSnackEdit />}
    >
        <SimpleForm>
            <TextField
                source="id"
                label="ID"
            />
            <TextInput
                source="text_de"
                label="Deutsche Übersetzung"
                validate={[required("Deutsche Übersetzung ist benötigt")]}
                fullWidth
            />
            <TextInput
                source="text_en"
                label="Englische Übersetzung"
                fullWidth

            />
            <ImageField
                source="image_url"
                label="Momentanes Bild"
            />
            <ImageInput
                source="new_image"
                accept="image/*"
                label="Bild">

                <ImageField
                    source="src"
                    title="Ausgewähltes Bild"
                />
            </ImageInput>
        </SimpleForm>
    </Edit>
);


export const snackCreate = props => (
    <Create {...props}
        mutationMode={"pessimistic"}
        aside={<AsideSnackCreate />}
    >
        <SimpleForm
            redirect="list"
        >
            <TextInput
                source="text_de"
                label="Deutsche Übersetzung"
                fullWidth
                validate={[required("Deutsche Übersetzung ist benötigt")]}
            />
            <TextInput
                source="text_en"
                label="Englische Übersetzung"
                fullWidth
            />
            <ImageInput
                source="new_image"
                accept="image/*"
                label="Bild"
            >
                <ImageField
                    source="src"
                    title="Ausgewähltes Bild"
                />
            </ImageInput>
        </SimpleForm>
    </Create>
);