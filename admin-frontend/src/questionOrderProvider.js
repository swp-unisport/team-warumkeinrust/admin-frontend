import { stringify } from 'query-string';
import {
    fetchUtils,
} from 'ra-core';


/*
DEFAULT DATA PROVIDER TEMPLATE
Just for copy and pasting when other data providers are needed.
*/


// export {
//     default as tokenAuthProvider,
//     fetchJsonWithAuthToken,
// } from './tokenAuthProvider';

// export {
//     default as jwtTokenAuthProvider,
//     fetchJsonWithAuthJWTToken,
// } from './jwtTokenAuthProvider';

const getPaginationQuery = (pagination) => {
    return {
        page: pagination.page,
        page_size: pagination.perPage,
    };
};

const getFilterQuery = (filter) => {
    const { q: search, ...otherSearchParams } = filter;
    return {
        ...otherSearchParams,
        search,
    };
};

export const getOrderingQuery = (sort) => {
    const { field, order } = sort;
    return {
        ordering: `${order === 'ASC' ? '' : '-'}${field}`,
    };
};

let cached_order = undefined;

export default (
    apiUrl,
    httpClient = fetchUtils.fetchJson
) => {
    const getOneJson = (resource, id) =>
        httpClient(`${apiUrl}/${resource}/${id}/`).then(
            (response) => response.json
        );

    return {
        getList: async (resource, params) => {
            const query = {
                ...getFilterQuery(params.filter),
                ...getPaginationQuery(params.pagination),
                ...getOrderingQuery(params.sort),
            };

            if (cached_order == undefined) {

                const url = `${apiUrl}/${resource}/?${stringify(query)}`;

                const { json } = await httpClient(url);

                cached_order = {
                    data: json.results,
                    total: json.count,
                }

                return {
                    data: json.results,
                    total: json.count,
                };

            } else {
                // Cache Already exist, so continue working on that
                return cached_order;
            }

        },

        getOne: async (resource, params) => {

            const data = cached_order.data[params.id - 1];
            return {
                data,
            };
        },

        getMany: (resource, params) => {
            return Promise.all(
                params.ids.map(id => getOneJson(resource, id))
            ).then(data => ({ data }));
        },

        getManyReference: async (resource, params) => {
            const query = {
                ...getFilterQuery(params.filter),
                ...getPaginationQuery(params.pagination),
                ...getOrderingQuery(params.sort),
                [params.target]: params.id,
            };
            const url = `${apiUrl}/${resource}/?${stringify(query)}`;

            const { json } = await httpClient(url);
            return {
                data: json.results,
                total: json.count,
            };
        },

        update: async (resource, params) => {

            let data = cached_order.data[params.id - 1];

            data.type = params.data.type;

            if (data.type !== "question") {
                data.question_id = null;
            } else {
                data.question_id = params.data.question_id;
            }

            cached_order.data[params.id - 1] = data;

            return { data: data };
        },

        updateMany: (resource, params) =>
            Promise.all(
                params.ids.map(id =>
                    httpClient(`${apiUrl}/${resource}/${id}/`, {
                        method: 'PATCH',
                        body: JSON.stringify(params.data),
                    })
                )
            ).then(responses => ({ data: responses.map(({ json }) => json.id) })),

        create: async (resource, params) => {
            const { json } = await httpClient(`${apiUrl}/${resource}/`, {
                method: 'POST',
                body: JSON.stringify(params.data),
            });
            return {
                data: { ...json },
            };
        },

        delete: async (resource, params) => {

            cached_order.data.splice(params.id - 1, 1);
            cached_order.total -= 1;

            for (let index = 0; index < cached_order.data.length; index++) {
                cached_order.data[index].id = index + 1;
            }

            return { data: {} };
        },


        deleteMany: (resource, params) =>
            Promise.all(
                params.ids.map(id =>
                    httpClient(`${apiUrl}/${resource}/${id}/`, {
                        method: 'DELETE',
                    })
                )
            ).then(responses => ({ data: responses.map(({ json }) => json.id) })),


        add_entry: async (resource, params) => {


            cached_order.data.push({
                id: cached_order.data.length + 1,
                type: "snack",
                question_id: null,
            });
            cached_order.total += 1;

            console.log(cached_order);

            return { data: cached_order.data[cached_order.data.length] };
        },

        move_entry: async (resource, params) => {

            let old_id = params.id - 1;
            let new_id = params.id - 1 + params.move;


            let obj = cached_order.data[old_id];
            let overwritten_obj = cached_order.data[new_id];

            let tmp = obj.id;
            obj.id = overwritten_obj.id;
            overwritten_obj.id = tmp;

            cached_order.data[new_id] = obj;
            cached_order.data[old_id] = overwritten_obj;

            console.log(cached_order)
            return { data: {} }
        },

        empty_cache: async (resource, params) => {

            // Call Fresh Data again to Overwrite Changes

            const url = `${apiUrl}/${resource}/`;

            const { json } = await httpClient(url);

            cached_order = {
                data: json.results,
                total: json.count,
            }


            return { data: {} };

        },

        save_order: async (resource, params) => {

            const { json } = await httpClient(`${apiUrl}/${resource}/`, {
                method: 'POST',
                body: JSON.stringify(cached_order.data),
            });

            return { data: json }

        },
    };
};
