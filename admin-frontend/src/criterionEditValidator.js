

export const criterionEditValidator = val => {

    if (val % 1 != 0 || !Number.isInteger(val)) {
        return "Muss ganzzahlig sein!";
    };

    if (val > 9) {
        return "Darf nicht größer als 9 sein.";
    };

    if (val < -1) {
        return "Nur -1 ist als negativer Wert erlaubt!";
    };

    if (val === 0) {
        return "Muss entweder -1 oder zwischen 1 und 9 liegen!";
    };

    return undefined;
};





// Used as a validator for our criteria. 
// Takes an input Int and corrects it if needed, returns correct value

export const criterionEditFormatter = val => {
    if (val === 0) {
        val = 1;
    } else if (val === null || val === undefined || val <= -2) {
        val = -1;
    } else if (val >= 10) {
        val = 9;
    };

    return Math.trunc(val);
};
