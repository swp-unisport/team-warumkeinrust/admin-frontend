import {
    List,
    Datagrid,
    TextField,
    ReferenceField,
    ChipField
} from 'react-admin';


import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'


const AsideCriterionList = () => (
    <div style={{
        width: 400,
        margin: '1em'
    }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>Hier wird jedes Kriterium aufgelistet</li>
                    <br />
                    <ul>
                        <li><i>Anzahl von aktiven Sportarten</i>: <br />Gibt an, wie viele Sportarten ein Rating größer als 1 für dieses Kriterium haben.</li>
                        <br />
                        <li><i>Summe der Gewichte</i>: <br />Gibt die Summe der Gewichte von aktiven Sportarten für dieses Kriterium an.</li>
                        <br />

                    </ul>

                </ul>

            </CardContent>
        </Card>
    </div>
);

export const criteriaList = props => (
    <List {...props}
        aside={<AsideCriterionList />}
        bulkActionButtons={false}>
        <Datagrid>

            <TextField source="id" sortable={false} />
            <TextField source="name" sortable={false} />

            <TextField
                source="number_of_sports_active"
                label="Anzahl von aktiven Sportarten"
                sortable={false} />

            <TextField
                source="sum_of_weights"
                label="Summe der Gewichte"
                sortable={false} />


            <ReferenceField source="question_id" reference="question" label="Frage" sortable={false}>
                <ChipField source="text_de" sortable={false} />
            </ReferenceField>

        </Datagrid>
    </List>
);
