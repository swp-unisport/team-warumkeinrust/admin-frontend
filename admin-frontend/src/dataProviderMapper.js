import drfProvider from 'ra-data-django-rest-framework';
import sportIncompleteProvider from './sportIncompleteProvider.js';
import scraperDataProvider from './scraperDataProvider.js';
import questionOrderProvider from './questionOrderProvider.js';
import activitySnackProvider from './activitySnackProvider.js';
import {
    GET_LIST,
    GET_ONE,
    CREATE,
    UPDATE,
    UPDATE_MANY,
    DELETE,
    GET_MANY,
    GET_MANY_REFERENCE,
    DELETE_MANY
} from 'react-admin';

// Mapping of data providers onto resource names
// For every call, it is checked which data provider should be used
// Depending on the need, further data providers can be defined and added
const dataProviders = [
    {
        dataProvider: drfProvider('http://localhost:8000/api/admin'),
        resources: ['sport', 'question', 'sport-archive', 'criteria'],
    },
    {
        dataProvider: sportIncompleteProvider('http://localhost:8000/api/admin'),
        resources: ['sport-incomplete'],
    },
    {
        dataProvider: scraperDataProvider('http://localhost:8000/api/admin'),
        resources: ['sport-scraper'],
    },
    {
        dataProvider: questionOrderProvider('http://localhost:8000/api/admin'),
        resources: ['question-order'],
    },
    {
        dataProvider: activitySnackProvider('http://localhost:8000/api/admin'),
        resources: ['snack', 'activity'],
    }
];

export default (type, resource, params) => {

    // Goes through the data provider list and takes the one fitting to the resource
    const dataProviderMapping = dataProviders.find((dp) =>
        dp.resources.includes(resource));

    // Maps the type of request onto the function of the data provider
    // "post_scraper" is a custom function to conform with the API endpoint which doesn't conform with normal CRUD operations
    const mappingType = {
        [GET_LIST]: 'getList',
        [GET_ONE]: 'getOne',
        [GET_MANY]: 'getMany',
        [GET_MANY_REFERENCE]: 'getManyReference',
        [CREATE]: 'create',
        [UPDATE]: 'update',
        [UPDATE_MANY]: 'updateMany',
        [DELETE]: 'delete',
        [DELETE_MANY]: 'deleteMany',
        ["post_scraper"]: 'post_scraper',
        ["add_entry"]: 'add_entry',
        ["move_entry"]: 'move_entry',
        ["save_order"]: 'save_order',
        ["empty_cache"]: 'empty_cache',
    };


    return dataProviderMapping.dataProvider[mappingType[type]](resource, params);
};