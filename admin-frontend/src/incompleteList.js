import * as React from 'react';
import {
    List,
    Datagrid,
    TextField,
    ArrayInput,
    SimpleFormIterator,
    NumberInput,
    Edit,
    SimpleForm,
    SaveButton,
    Toolbar,
    FormDataConsumer,
    ReferenceField,
    ChipField,
    useNotify,
    useRefresh
} from 'react-admin';

import { criterionEditValidator } from './criterionEditValidator.js';

import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'


const AsideIncompleteList = () => (
    <div style={{
        width: 400,
        margin: '1em'
    }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>Klicke auf die ID von dem Sport, um den gesamten Sport zu bearbeiten.</li>
                    <br />
                    <li>Klicke auf eine Reihe, um diese aufzuklappen. Hier können die bisher noch nicht angegebenen Kriterienwertungen ausfüllt werden.</li>
                    <br />
                    <li><b>Die Zahl -1 bedeutet, dass das Kriterium nicht bewertet worden ist.</b></li>
                    <br />
                    <li>Wenn ein Sport vollständig bewertet ist, verschwindet er von dieser Liste.</li>
                    <br />
                    <li>Nach dem Bearbeiten: Speichern nicht vergessen!</li>
                </ul>

            </CardContent>
        </Card>
    </div>
);




// Eliminates the delete function while editing the incomplete list
const IncompleteEditToolbar = props => (
    <Toolbar {...props} >
        <SaveButton />
    </Toolbar>
);


export const IncompleteList = props => (
    <List
        {...props}
        aside={<AsideIncompleteList />}
        bulkActionButtons={false}>
        {/* rowClick expand means that no edit page is opened, but instead shown below the data row itself without reloading */}
        <Datagrid
            rowClick="expand"
            expand={IncompleteEdit}
            style={{ tableLayout: "fixed", }} >

            {/* Reference Field fetches every Sport object if a reference field to it exists.
            This means that n GETs are made for one load of the incomplete-list.
            Could probably be improved in the future.
             */}
            <ReferenceField
                label="ID"
                source="id"
                reference="sport"
                sortable={false}>
                <ChipField source="id" />
            </ReferenceField>

            <TextField
                source="name"
                sortable={false}
            />
        </Datagrid>
    </List>
);


export const IncompleteEdit = props => {
    const notify = useNotify();
    const refresh = useRefresh();

    // Redefining onSuccess for following behaviour:
    // Entering criteria values, but some are left as -1? => Page stays on focus with the edit
    // Entering all valid criteria values? => Pages refreshes and the row vanishes
    const onSuccess = () => {
        notify((`Kriterien eingetragen`));
        refresh();

    }
    // mutationMode pessimistic deactivates the optimistic rendering
    // Now the request will be sent first and rendered after getting the return code
    return (
        <Edit
            {...props}
            mutationMode="pessimistic"
            onSuccess={onSuccess}
            title={" "}
        >
            <SimpleForm toolbar={<IncompleteEditToolbar />}>

                <Typography
                    variant="h5"
                    fullWidth
                >
                    Fehlende Kriterien ausfüllen
                </Typography>

                {/* Documentation for formatting https://marmelab.com/react-admin/CreateEdit.html#custom-row-container */}
                <TextField source="id" />
                <TextField source="name" />

                <ArrayInput
                    source="criteria"
                    label=""
                >
                    <SimpleFormIterator
                        disableAdd
                        disableRemove
                    >

                        {/* Documentation is wrong, look here! https://github.com/marmelab/react-admin/issues/2500 
                    getSource() needs to get called without arguments before the return,
                    instead of as the source parameter of the TextField with "name" as argument */}
                        <FormDataConsumer>
                            {({ getSource, scopedFormData }) => {
                                getSource();
                                return (
                                    <TextField
                                        source={"name"} record={scopedFormData} label="Kriterienname"
                                    />
                                );
                            }}

                        </FormDataConsumer>

                        <NumberInput
                            source="value"
                            label="Wertung"
                            validate={[criterionEditValidator]}
                        />
                    </SimpleFormIterator>
                </ArrayInput>
            </SimpleForm>
        </Edit>
    );
};
