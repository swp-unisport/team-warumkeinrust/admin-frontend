import {
    List,
    Datagrid,
    UrlField,
    BooleanField,
    Edit,
    SimpleForm,
    TextInput,
    ArrayInput,
    BooleanInput,
    DateInput,
    TextField,
    SimpleFormIterator,
    NumberInput,
    Create,
    FormDataConsumer
} from 'react-admin';

import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'



import { criterionEditValidator } from './criterionEditValidator.js';

const AsideSportList = () => (
    <div style={{
        width: 400,
        margin: '1em'
    }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>Klicke auf eine Datenreihe, um den Eintrag zu bearbeiten.</li>
                    <br />
                    <li>Neue Sportarten können durch "Create" hinzugefügt werden.</li>
                    <br />
                    <li>Die Sportarten können (momentan noch ohne Kriterien) über "Export" als .csv-Datei heruntergeladen werden.</li> {/* Das Implementieren */}
                </ul>

            </CardContent>
        </Card>
    </div>
);

const AsideSportEdit = () => (
    <div style={{
        width: 400,
        margin: '1em'
    }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>Der Slider "Wird aktuell angeboten" entscheidet, ob der Sport beim Quiz als Ergebnis auftreten kann.</li>
                    <br />
                    <li>Wenn "Wird aktuell angeboten" deaktiviert wird,
                        rutscht der Sport in das Archiv und wird im Sport Management oder der Incomplete List nicht mehr angezeigt.</li>
                    <br />
                    <li>Das Feld "Zuletzt genutzt" sagt aus, wann der Sport zu der aktiven Auswahl hinzugefügt worden ist.
                        Das passiert entweder durch manuelles Aktivieren oder durch das Scrapen von neuen Informationen von <a href="https://www.buchsys.de/fu-berlin/angebote/aktueller_zeitraum/index.html">buchsys.de</a>.</li>
                    <br />
                    <li>
                        <p>Für jedes Kriterium kann ein ganzzahliger Wert zwischen 1 und 9 eingesetzt werden.
                            Das ist die Gewichtung des Sports in dem jeweiligen Kriterium.
                        </p>
                        <p>
                            Es kann auch eine -1 für das Kriterium eingetragen werden.
                            <b> Das bedeutet, dass das Kriterium als nicht ausgefüllt gilt und in der Incomplete Sport List erscheinen wird.</b>
                        </p>
                    </li>

                </ul>

            </CardContent>
        </Card>
    </div>
);

const AsideSportCreate = () => (
    <div style={{ width: 400, margin: '1em' }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>Die Kriterien können ausgefüllt werden, nachdem der Sport erstellt wurde.
                        Du wirst automatisch auf die richtige Seite weitergeleitet.</li>
                </ul>

            </CardContent>
        </Card>
    </div>
);

export const SportList = props => (
    <List {...props} aside={<AsideSportList />}>
        <Datagrid
            rowClick="edit"
            style={{
                tableLayout: "fixed",
            }}>

            <TextField source="id" />
            <TextField source="name" />
            <UrlField source="url" />
            <BooleanField
                source="is_filled"
                label="Kriterien ausgefüllt?"
            />
        </Datagrid>
    </List>
);



export const SportEdit = props => (
    <Edit
        {...props}
        aside={<AsideSportEdit />}
        mutationMode='pessimistic'
    >
        <SimpleForm>

            <Typography
                variant="h4"
                fullWidth
            >
                Sport bearbeiten
            </Typography>
            <TextInput
                disabled source="id"
                label="ID"
            />
            <TextInput
                source="name"
                fullWidth
                required
            />
            <TextInput
                source="url"
                label="URL"
                fullWidth
                required
            />
            <BooleanInput
                source="currently_active"
                label="Wird aktuell angeboten"
            />
            <DateInput
                disabled
                source="last_used"
                label="Zuletzt genutzt"
            />

            <Typography
                variant="h5"
                align="left"
            >
                Kriterien
            </Typography>

            <ArrayInput
                source="criteria"
                label=""
            >
                <SimpleFormIterator
                    disableAdd
                    disableRemove
                >  {/* Used because you cannot know how many objects will be sent */}

                    {/* Documentation is wrong, look here! https://github.com/marmelab/react-admin/issues/2500 
                    getSource() needs to get called without arguments before the return,
                    instead of as the source parameter of the TextField with "name" as argument */}
                    <FormDataConsumer>
                        {({ getSource, scopedFormData }) => {
                            getSource();
                            return (
                                <TextField
                                    source={"name"} record={scopedFormData} label="Kriterienname"
                                />
                            );
                        }}
                    </FormDataConsumer>


                    {/* criterionEditValidator checks input if it is within 1-9 or -1 and sets it accordingly */}
                    <NumberInput
                        source="value"
                        validate={[criterionEditValidator]}
                        label="Kriterienwertung"
                    />
                </SimpleFormIterator>
            </ArrayInput>
        </SimpleForm>
    </Edit>
);

// sportCreate cannot know at this point how many and which criteria are used, so they are editable after creating the sport
export const SportCreate = props => (
    <Create {...props} aside={<AsideSportCreate />}>
        <SimpleForm>
            <Typography
                variant="h4"
                align="left"
            >
                Sport erstellen
            </Typography>
            <TextInput
                source="name"
                fullWidth
                required
            />
            <TextInput
                source="url"
                fullWidth
                required
            />
        </SimpleForm>
    </Create>
);