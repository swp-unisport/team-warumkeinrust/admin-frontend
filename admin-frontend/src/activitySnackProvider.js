import { stringify } from 'query-string';
import {
    fetchUtils,
} from 'ra-core';
import dataProviderMapper from './dataProviderMapper';

/*
DEFAULT DATA PROVIDER TEMPLATE
Just for copy and pasting when other data providers are needed.
*/


// export {
//     default as tokenAuthProvider,
//     fetchJsonWithAuthToken,
// } from './tokenAuthProvider';

// export {
//     default as jwtTokenAuthProvider,
//     fetchJsonWithAuthJWTToken,
// } from './jwtTokenAuthProvider';

const getPaginationQuery = (pagination) => {
    return {
        page: pagination.page,
        page_size: pagination.perPage,
    };
};

const getFilterQuery = (filter) => {
    const { q: search, ...otherSearchParams } = filter;
    return {
        ...otherSearchParams,
        search,
    };
};

export const getOrderingQuery = (sort) => {
    const { field, order } = sort;
    return {
        ordering: `${order === 'ASC' ? '' : '-'}${field}`,
    };
};

export default (
    apiUrl,
    httpClient = fetchUtils.fetchJson
) => {
    const getOneJson = (resource, id) =>
        httpClient(`${apiUrl}/${resource}/${id}/`).then(
            (response) => response.json
        );

    return {
        getList: async (resource, params) => {
            const query = {
                ...getFilterQuery(params.filter),
                ...getPaginationQuery(params.pagination),
                ...getOrderingQuery(params.sort),
            };
            const url = `${apiUrl}/${resource}/?${stringify(query)}`;

            const { json } = await httpClient(url);

            return {
                data: json.results,
                total: json.count,
            };
        },

        getOne: async (resource, params) => {
            const data = await getOneJson(resource, params.id);
            return {
                data
            };
        },

        getMany: (resource, params) => {
            return Promise.all(
                params.ids.map(id => getOneJson(resource, id))
            ).then(data => ({ data }));
        },

        getManyReference: async (resource, params) => {
            const query = {
                ...getFilterQuery(params.filter),
                ...getPaginationQuery(params.pagination),
                ...getOrderingQuery(params.sort),
                [params.target]: params.id,
            };
            const url = `${apiUrl}/${resource}/?${stringify(query)}`;

            const { json } = await httpClient(url);
            return {
                data: json.results,
                total: json.count,
            };
        },

        update: async (resource, params) => {


            let data = {
                text_de: params.data.text_de,
                text_en: params.data.text_en,
            }

            if (params.data.new_image !== undefined) {

                let image = params.data.new_image;
                let image64 = await convertFileToBase64(image);

                let new_image_type = params.data.new_image.rawFile.type;

                data = {
                    ...data,
                    image_type: new_image_type,
                    image: image64
                }
            }

            const { json } = await httpClient(`${apiUrl}/${resource}/${params.id}/`, {
                method: 'PATCH',
                body: JSON.stringify(
                    data
                ),
            });
            return { data: json };
        },

        updateMany: (resource, params) =>
            Promise.all(
                params.ids.map(id =>
                    httpClient(`${apiUrl}/${resource}/${id}/`, {
                        method: 'PATCH',
                        body: JSON.stringify(params.data),
                    })
                )
            ).then(responses => ({ data: responses.map(({ json }) => json.id) })),

        create: async (resource, params) => {


            let data = {
                text_de: params.data.text_de,
                text_en: params.data.text_en,
            }

            if (params.data.new_image !== undefined) {

                let image = params.data.new_image;
                let image64 = await convertFileToBase64(image);

                let new_image_type = params.data.new_image.rawFile.type;

                data = {
                    ...data,
                    image_type: new_image_type,
                    image: image64
                }
            }

            const { json } = await httpClient(`${apiUrl}/${resource}/`, {
                method: 'POST',
                body: JSON.stringify(data),
            });
            return {
                data: { ...json },
            };
        },

        delete: (resource, params) =>
            httpClient(`${apiUrl}/${resource}/${params.id}/`, {
                method: 'DELETE',
            }).then(() => ({ data: params.previousData })),

        deleteMany: (resource, params) =>
            Promise.all(
                params.ids.map(id =>
                    httpClient(`${apiUrl}/${resource}/${id}/`, {
                        method: 'DELETE',
                    })
                )
            ).then(responses => ({ data: responses.map(({ json }) => json.id) })),
    };
};


const convertFileToBase64 = file =>
    new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => resolve(reader.result);
        reader.onerror = reject;

        reader.readAsDataURL(file.rawFile);
    });