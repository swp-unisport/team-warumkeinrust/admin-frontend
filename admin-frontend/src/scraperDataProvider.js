import { stringify } from 'query-string';
import {
    fetchUtils,
} from 'ra-core';


const getPaginationQuery = (pagination) => {
    return {
        page: pagination.page,
        page_size: pagination.perPage,
    };
};

const getFilterQuery = (filter) => {
    const { q: search, ...otherSearchParams } = filter;
    return {
        ...otherSearchParams,
        search,
    };
};

export const getOrderingQuery = (sort) => {
    const { field, order } = sort;
    return {
        ordering: `${order === 'ASC' ? '' : '-'}${field}`,
    };
};


/*
WHAT HAS CHANGED IN THIS DATA PROVIDER
This Data Provider is meant for use of the Sport Scraper.
The only thing changed here is the "create" function. It has been replaced with a "post_scraper" function, which sends every ticked row to the API.
The change of the function name has been done because react-admin demands a certain kind of answer with the created object.
Since this isn't given by the scraper (completely different usecase after all), we had to circumvent this check.

Since the data of the checked rows isn't given, but only the IDs, we need to cache the fetched list of diffs so that it can still be accessed by post_scraper.
Same trick as in incompleteListProvider, we just save it in a local variable.
*/



let cached_scraped_sport_list = []

export default (
    apiUrl,
    httpClient = fetchUtils.fetchJson
) => {
    const getOneJson = (resource, id) =>
        httpClient(`${apiUrl}/${resource}/${id}/`).then(
            (response) => response.json
        );

    return {
        getList: async (resource, params) => {
            const query = {
                ...getFilterQuery(params.filter),
                ...getPaginationQuery(params.pagination),
                ...getOrderingQuery(params.sort),
            };
            const url = `${apiUrl}/${resource}/?${stringify(query)}`;

            const { json } = await httpClient(url);

            cached_scraped_sport_list = json.results;

            return {
                data: json.results,
                total: json.count,
            };
        },

        getOne: async (resource, params) => {
            const data = await getOneJson(resource, params.id);
            return {
                data,
            };
        },

        getMany: (resource, params) => {
            return Promise.all(
                params.ids.map(id => getOneJson(resource, id))
            ).then(data => ({ data }));
        },

        getManyReference: async (resource, params) => {
            const query = {
                ...getFilterQuery(params.filter),
                ...getPaginationQuery(params.pagination),
                ...getOrderingQuery(params.sort),
                [params.target]: params.id,
            };
            const url = `${apiUrl}/${resource}/?${stringify(query)}`;

            const { json } = await httpClient(url);
            return {
                data: json.results,
                total: json.count,
            };
        },

        update: async (resource, params) => {
            const { json } = await httpClient(`${apiUrl}/${resource}/${params.id}/`, {
                method: 'PATCH',
                body: JSON.stringify(params.data),
            });
            return { data: json };
        },

        updateMany: (resource, params) =>
            Promise.all(
                params.ids.map(id =>
                    httpClient(`${apiUrl}/${resource}/${id}/`, {
                        method: 'PATCH',
                        body: JSON.stringify(params.data),
                    })
                )
            ).then(responses => ({ data: responses.map(({ json }) => json.id) })),

        post_scraper: async (resource, params) => {

            let sport_list;

            if (cached_scraped_sport_list === []) {
                sport_list = this.getList(resource, { filter: {}, pagination: { page: 1, perPage: 1000 }, sort: { field: "id", order: "ASC" } });
            } else {
                sport_list = cached_scraped_sport_list;
            }

            let checked_sport_list = [];

            params.forEach(id => {
                checked_sport_list.push(sport_list[id - 1]);
            });

            const { json } = await httpClient(`${apiUrl}/sport-scraper/`, {
                method: 'POST',
                body: JSON.stringify(checked_sport_list),
            });

            return {
                data: { ...json },
            };
        },

        delete: (resource, params) =>
            httpClient(`${apiUrl}/${resource}/${params.id}/`, {
                method: 'DELETE',
            }).then(() => ({ data: params.previousData })),

        deleteMany: (resource, params) =>
            Promise.all(
                params.ids.map(id =>
                    httpClient(`${apiUrl}/${resource}/${id}/`, {
                        method: 'DELETE',
                    })
                )
            ).then(responses => ({ data: responses.map(({ json }) => json.id) })),
    };
};
