import * as React from 'react';
import {
    List,
    Datagrid,
    TextField,
    Edit,
    SimpleForm,
    TextInput,
    Create,
    NumberField
} from 'react-admin';

import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'


const AsideQuestionList = () => (
    <div style={{ width: 400, margin: '1em' }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>Klicke auf eine Datenreihe, um sie zu bearbeiten.</li>
                    <br />
                    <li>Fragen müssen eine deutsche Übersetzung und können eine englische Übersetzung haben.</li>
                    <br />
                    <li>Kriterien sind einzigartig. Das heißt, dass ein Kriterium nicht doppelt vergeben werden kann!</li>
                    <br />
                    <li>Wenn eine Frage mit ihrem Kriterium gelöscht wird, gehen auch alle Gewichtungen zu diesem Kriterium verloren!</li>
                </ul>

            </CardContent>
        </Card>
    </div>
);

const AsideQuestionEdit = () => (
    <div style={{ width: 400, margin: '1em' }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>Pflichtangaben sind:

                        <ul>
                            <li>Deutscher Fragetext</li>
                            <li>Name des Kriteriums</li>
                        </ul>

                    </li>
                    <br />
                    <li>Wenn ein Kriterium umbenannt wird, wird es automatisch bei allen Sportarten aktualisiert.</li>
                    <br />
                    <li>Kriterien sind einzigartig. Das heißt, dass ein Kriterium nicht doppelt vergeben werden kann!</li>
                </ul>

            </CardContent>
        </Card>
    </div>
);

const AsideQuestionCreate = () => (
    <div style={{
        width: 400,
        margin: '1em'
    }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>Pflichtangaben sind:

                        <ul>
                            <li>Deutscher Fragetext</li>
                            <li>Name des Kriteriums</li>
                        </ul>

                    </li>
                    <br />
                    <li>Kriterien sind einzigartig. Das heißt, dass ein Kriterium nicht doppelt vergeben werden kann!
                        Hier muss also ein neuer Kriterienname angegeben werden.</li>
                </ul>

            </CardContent>
        </Card>
    </div>
);


export const QuestionList = props => (
    <List
        {...props}
        aside={<AsideQuestionList />}
    >
        <Datagrid
            rowClick="edit"
        >

            <NumberField
                source="id"
                label="ID"
                sortable={false}
            />

            <TextField
                source="text_de"
                label="Deutscher Fragetext"
                sortable={false}
            />
            <TextField
                source="text_en"
                label="Englischer Fragetext"
                sortable={false}
            />
            <TextField
                source="criterion"
                label="Name des Kriteriums"
                sortable={false}
            />
        </Datagrid>
    </List>
);

// mutationMode pessimistic deactivates the optimistic rendering
// Now the request will be sent first and rendered after getting the return code
export const QuestionEdit = props => (
    <Edit
        {...props}
        mutationMode="undoable"
        aside={<AsideQuestionEdit />}
    >
        <SimpleForm>
            <Typography
                ariant="h5"
                fullWidth
            >
                Frage bearbeiten
            </Typography>
            <TextInput
                source="text_de"
                label="Deutscher Fragetext"
                fullWidth
                required
            />
            <TextInput
                source="text_en"
                label="Englischer Fragetext"
                fullWidth
            />
            <TextInput
                source="criterion"
                label="Name des Kriteriums"
                required
            />
        </SimpleForm>
    </Edit>
);

export const QuestionCreate = props => (

    <Create {...props} aside={<AsideQuestionCreate />}>
        <SimpleForm redirect="list">
            <Typography
                variant="h5"
                fullWidth
            >
                Frage erstellen
            </Typography>

            <TextInput
                source="text_de"
                label="Deutscher Fragetext"
                fullWidth
                required
            />
            <TextInput
                source="text_en"
                label="Englischer Fragetext"
                fullWidth
            />
            <TextInput
                source="criterion"
                label="Name des Kriteriums"
                required
            />
        </SimpleForm>
    </Create>
);