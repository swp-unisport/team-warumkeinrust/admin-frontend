import {
    List,
    Datagrid,
    TextField,
    TopToolbar,
    Button,
    useNotify,
    Edit,
    SimpleForm,
    ReferenceInput,
    SelectInput,
    Toolbar,
    SaveButton,
    ReferenceField,
    useDataProvider,
    RadioButtonGroupInput,
    useRefresh,
    useListContext,
    Pagination,
} from 'react-admin';

import Chip from '@material-ui/core/Chip';
import { makeStyles } from '@material-ui/core/styles';


import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

const PostShowActions = ({ basePath, data, resource }) => {
    const notify = useNotify();
    const refresh = useRefresh();
    const dataProvider = useDataProvider();

    const addOrderEntry = () => {
        dataProvider.add_entry("question-order", {}).then(
            response => {
                notify("Neuen Entry Hinzugefügt!");
                refresh();
            }).catch(error => {
                notify("Etwas ist schief gelaufen!", "warning");
            });

    }

    const save_order = () => {
        dataProvider.save_order("question-order", {}).then(
            response => {
                notify("Veränderungen gespeichert!");
                refresh();
            }).catch(error => {
                notify("Etwas ist schief gelaufen!", "warning");
            });

    }

    const empty_cache = () => {
        dataProvider.empty_cache("question-order", {}).then(
            response => {
                notify("Veränderungen auf Serverzustand zurückgesetzt!");
                refresh();
            }).catch(error => {
                notify("Etwas ist schief gelaufen!", "warning");
            });

    }

    return (
        <TopToolbar>
            <Button
                color="primary"
                onClick={addOrderEntry}
                label="Eintrag Hinzufügen"
            />
            <Button
                color="primary"
                onClick={save_order}
                label="Veränderungen Speichern"
            />

            <Button
                color="primary"
                onClick={empty_cache}
                label="Veränderungen Verwerfen"
            />

        </TopToolbar>
    )
};


const AsideQuestionOrder = () => (
    <div style={{ width: 400, margin: '1em' }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />

                <ul>
                    <li>
                        Jeder Eintrag in dieser Liste ist eine Seite im Quiz, die entweder Fragen, Aktivitäten, oder Wissenssnacks anzeigt.
                        Wenn die Zeilen aufgeklappt werden, dann kann sowohl die Art des Eintrags, als auch die Frage selbst ausgewählt werden.
                    </li>
                    <br />
                    <li>
                        Um die einzelnen Änderungen an einem Eintrag zu speichern, muss "Save" in dem aufgeklappten Feld gedrückt werden.
                        Dort kann man ebenso den Eintrag löschen, wenn man möchte.
                    </li>
                    <br />
                    <li>
                        <p><b>Um die Reihenfolge zu speichern, muss der Knopf "Veränderungen Speichern" oben rechts gedrückt werden.</b></p>
                        <p>Mit dem Knopf "Veränderungen Verwerfen" werden die lokalen Änderungen auf den letzten Stand zurückgesetzt.</p>
                        <p>Mit dem Knopf "Eintrag Hinzufügen" kann ein neuer Eintrag am Ende der Liste angehangen werden.</p>
                    </li>
                    <br />
                    <li>
                        Mit den Knöpfen "Hoch" und "Runter" können die Einträge um jeweils eine Zeile verschoben werden.
                    </li>
                    <br />
                    <li>
                        In jedem Eintrag vom Typ Frage muss ein Fragentext ausgewählt sein, bevor die Liste gespeichert werden kann.
                    </li>

                </ul>

            </CardContent>
        </Card>
    </div>
);

const DirEditButton = props => {
    const notify = useNotify();
    const refresh = useRefresh();
    const dataProvider = useDataProvider();
    const listContext = useListContext();

    if (props.record === undefined) {
        return null;
    };

    if (props.record.id + props.direction > listContext.total || props.record.id + props.direction < 1) {
        return null;
    };

    const id = props.record.id;
    const dir = props.direction;


    const action = (props) => {

        dataProvider.move_entry('question-order', { id: id, move: dir }).then(
            response => {
                notify("Entry Verschoben!");
                refresh();
            }).catch(error => {
                notify("Etwas ist schief gelaufen!", "warning");
            });

    };

    return <Button label={props.custom_label} onClick={action} />
}

const useStyles = makeStyles({
    question: { color: 'white', background: 'blue' },
    snack: { color: 'black', background: 'lightGreen' },
    activity: { color: 'white', background: 'darkRed' },
});

const QuestionTypeChip = props => {
    const classes = useStyles();

    if (props.record === undefined) {
        return null;
    };


    let verbose_type = "";
    if (props.record.type === "question") {
        verbose_type = "Frage";
    } else if (props.record.type === "snack") {
        verbose_type = "Wissenssnack";
    } else {
        verbose_type = "Aktivität";
    };

    return (
        <Chip label={verbose_type} className={classes[props.record.type]} />
    );
};




const QuestionOrderPagination = props => <Pagination rowsPerPageOptions={[]} {...props} />;

export const orderList = props => (


    <List
        {...props}
        actions={<PostShowActions />}
        aside={<AsideQuestionOrder />}
        pagination={<QuestionOrderPagination />}
        perPage={1000}
        bulkActionButtons={false}
        empty={false}
    >
        <Datagrid
            expand={orderEdit}
        >
            <TextField
                label="Reihenfolge"
                source="id"
                sortable={false}
            />
            <QuestionTypeChip
                label="Eintragstyp"
                source="type"
                sortable={false}
            />

            <TextField
                label="Fragen-ID"
                source="question_id"
                sortable={false}
            />
            <ReferenceField
                label="Fragentext"
                reference="question"
                source="question_id"
                sortable={false}
            >
                <TextField source="text_de" />
            </ReferenceField>

            <DirEditButton direction={-1} custom_label="Hoch" sortable={false} />

            <DirEditButton direction={+1} custom_label="Runter" sortable={false} />

        </Datagrid>
    </List>

);

const OrderEditToolbar = props => {
    const notify = useNotify();
    const refresh = useRefresh();
    const dataProvider = useDataProvider();

    const deleteEntry = () => {
        dataProvider.delete("question-order", { id: props.record.id }).then(
            response => {
                notify("Eintrag erfolgreich gelöscht!");
                refresh();
            }).catch(error => {
                notify("Etwas ist schief gelaufen!", "warning");
            });
    };

    return (
        <Toolbar {...props} >
            <SaveButton />
            <Button
                color="primary"
                onClick={deleteEntry}
                label="Eintrag Löschen"
            />
        </Toolbar>
    )
};

const CategorySensitiveQuestionField = (props) => {
    if (props.record.type === "question") {
        return (
            <ReferenceInput
                label="Frage"
                reference="question"
                source="question_id"
                fullWidth
            >
                <SelectInput
                    optionText="text_de"
                    fullWidth
                />
            </ReferenceInput>
        )
    } else {
        return null
    }
}


export const orderEdit = props => (
    <Edit {...props} mutationMode={"optimistic"} title={" "}>
        <SimpleForm toolbar={<OrderEditToolbar />}>

            <RadioButtonGroupInput
                source="type"
                label="Eintragsart"
                choices={[
                    { id: "snack", name: "Wissenssnack" },
                    { id: "activity", name: "Aktivität" },
                    { id: "question", name: "Frage" }]}
            />
            <CategorySensitiveQuestionField />

        </SimpleForm>
    </Edit>
);