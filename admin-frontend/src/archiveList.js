import {
    List,
    Datagrid,
    UrlField,
    TextField,
    DateField,
    Button,
    useDataProvider,
    useNotify,
    useRefresh,
} from 'react-admin';


import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'

const ArchiveCriterionList = () => (
    <div style={{
        width: 400,
        margin: '1em'
    }}>
        <Card>
            <CardContent>
                <Typography
                    variant="h4"
                    align="center"
                >
                    Tipps&Tricks
                </Typography>
                <br />
                <ul>
                    <li>Dies ist das Archiv für inaktive Sportarten.</li>
                    <br />
                    <li>Dazu zählen Sportarten, die beim letzten Auslesen von <a href="https://www.buchsys.de/fu-berlin/angebote/aktueller_zeitraum/index.html">buchsys.de</a> nicht mehr erkannt worden sind und deswegen aus der Liste von aktiven Sportarten genommen wurden.</li>
                    <br />
                    <li>Die Sportarten können trotzdem manuell per Knopfdruck wieder der aktiven Sportartenliste zugeordnet werden.</li>
                    <br />
                </ul>

            </CardContent>
        </Card>
    </div>
);


const ActivateButton = props => {
    const notify = useNotify();
    const refresh = useRefresh();
    const dataProvider = useDataProvider();

    let id = props.record.id;

    const activate_sport = props => {
        dataProvider.update('sport', { id: id, data: { currently_active: true } }).then(
            response => {
                notify("Erfolgreich Aktiviert!");
                refresh();
            }).catch(error => {
                notify("Etwas ist schief gelaufen!", "warning");
            });

    }

    return <Button label="Aktivieren!" onClick={activate_sport} {...props} />
}

export const archiveList = props => (
    <List {...props}
        aside={<ArchiveCriterionList />}
        bulkActionButtons={false}>
        <Datagrid>
            <TextField source="id" sortable={false} />
            <TextField source="name" sortable={false} />
            <UrlField source="url" sortable={false} />
            <DateField source="last_used" sortable={false} />

            <ActivateButton />
        </Datagrid>
    </List>
);
