import { stringify } from 'query-string';
import {
    fetchUtils,
} from 'ra-core';

/*
DATA PROVIDER FOR INCOMPLETE LIST
Based on the default Django REST data provider https://github.com/bmihelac/ra-data-django-rest-framework

Changed here:
- getList:
    Saves the returned data from the backend in the cached_incomplete variable for further use
- getOne:
    Checks cached_incomplete before asking the server for the complete list of all incomplete Sports
- update:
    Sends filled out criteria to sport/id instead of sport-incomplete/id, since that doesn't exist
    Also, the function returns only the other incomplete criteria instead of all

It might be worth thinking about changing the API in the backend so that sport-incomplete is a whole Viewset
This would bloat the API but make handling in the frontend easier.

*/


// export {
//     default as jwtTokenAuthProvider,
//     fetchJsonWithAuthJWTToken,
// } from './jwtTokenAuthProvider';

const getPaginationQuery = (pagination) => {
    return {
        page: pagination.page,
        page_size: pagination.perPage,
    };
};

const getFilterQuery = (filter) => {
    const { q: search, ...otherSearchParams } = filter;
    return {
        ...otherSearchParams,
        search,
    };
};

export const getOrderingQuery = (sort) => {
    const { field, order } = sort;
    return {
        ordering: `${order === 'ASC' ? '' : '-'}${field}`,
    };
};

// Cache of the Incomplete List because they can only be called all at once, making getOne very unoptimized
let cached_incomplete = {};

export default (
    apiUrl,
    httpClient = fetchUtils.fetchJson
) => {
    const getOneJson = (resource, id) =>
        httpClient(`${apiUrl}/${resource}/${id}/`).then(
            (response) => response.json
        );

    return {
        getList: async (resource, params) => {
            const query = {
                ...getFilterQuery(params.filter),
                ...getPaginationQuery(params.pagination),
                ...getOrderingQuery(params.sort),
            };
            const url = `${apiUrl}/${resource}/?${stringify(query)}`;

            const { json } = await httpClient(url);

            // Caches all results
            cached_incomplete = json.results;

            return {
                data: json.results,
                total: json.count,
            };
        },

        getOne: async (resource, params) => {

            let data = {}

            // Checks if the data is already present in the variable cache, so it doesn't need to call everything
            if (cached_incomplete == {}) {

                const url = `${apiUrl}/${resource}/`;
                const { json } = await httpClient(url);

                data = json.results.find((data) => data.id == params.id);
            } else {
                data = cached_incomplete.find((data) => data.id == params.id);
            }

            let formatted_criteria = [];
            data.criteria.forEach(criterion => {
                formatted_criteria.push(
                    {
                        "id": criterion.id,
                        "name": criterion.name,
                        "value": -1
                    }
                );
            });
            data.criteria = formatted_criteria;

            return {
                data
            };
        },

        getMany: (resource, params) => {
            return Promise.all(
                params.ids.map(id => getOneJson(resource, id))
            ).then(data => ({ data }));
        },

        getManyReference: async (resource, params) => {
            const query = {
                ...getFilterQuery(params.filter),
                ...getPaginationQuery(params.pagination),
                ...getOrderingQuery(params.sort),
                [params.target]: params.id,
            };
            const url = `${apiUrl}/${resource}/?${stringify(query)}`;

            const { json } = await httpClient(url);
            return {
                data: json.results,
                total: json.count,
            };
        },

        update: async (resource, params) => {

            const verbose_criteria_list = [];

            // Filter the criteria that were filled out
            params.data.criteria.forEach(criterion => {
                if (criterion.value !== -1) {
                    verbose_criteria_list.push({
                        "id": criterion.id,
                        "value": criterion.value
                    });
                };
            });

            // Send criteria that were filled out
            const data = {
                "criteria": verbose_criteria_list
            };

            const { json } = await httpClient(`${apiUrl}/sport/${params.id}/`, {
                method: 'PATCH',
                body: JSON.stringify(data),
            });

            // The whole Sport object has been returned
            // Just return the unreturned criteria, or None if everything is filled out

            let new_criteria = []
            json.criteria.forEach(element => {
                if (element.value <= -1) {
                    new_criteria.push(element)
                }
            });

            // At the moment, even sports with everything filled in are still shown
            // At a future iteration, it may be useful to remove it from the shown list
            json.criteria = new_criteria;
            return { data: json };

        },

        updateMany: (resource, params) =>
            Promise.all(
                params.ids.map(id =>
                    httpClient(`${apiUrl}/${resource}/${id}/`, {
                        method: 'PATCH',
                        body: JSON.stringify(params.data),
                    })
                )
            ).then(responses => ({ data: responses.map(({ json }) => json.id) })),

        create: async (resource, params) => {
            const { json } = await httpClient(`${apiUrl}/${resource}/`, {
                method: 'POST',
                body: JSON.stringify(params.data),
            });
            return {
                data: { ...json },
            };
        },

        delete: (resource, params) =>
            httpClient(`${apiUrl}/${resource}/${params.id}/`, {
                method: 'DELETE',
            }).then(() => ({ data: params.previousData })),

        deleteMany: (resource, params) =>
            Promise.all(
                params.ids.map(id =>
                    httpClient(`${apiUrl}/${resource}/${id}/`, {
                        method: 'DELETE',
                    })
                )
            ).then(responses => ({ data: responses.map(({ json }) => json.id) })),
    };
};
