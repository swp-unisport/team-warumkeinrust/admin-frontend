import * as React from 'react';
import { useDataProvider, useNotify, useRedirect, Button } from 'react-admin';

export const PostScraperButton = ({ selectedIds }) => {
    const notify = useNotify();
    const redirect = useRedirect();
    const dataProvider = useDataProvider();

    const postToScraper = () => dataProvider.post_scraper('sport-scraper', selectedIds).then(
        response => {
            notify("Ausgewählte Sportarten wurden eingefügt");
            redirect("sport-incomplete/")
        }).catch(error => {
            notify("Etwas ist schief gelaufen!", "warning");
        });

    return <Button label="Ausgewählte Sportarten hinzufügen" onClick={postToScraper} />
};

export default PostScraperButton