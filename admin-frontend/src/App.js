import * as React from 'react';

import { Admin, Resource } from 'react-admin';
import dataProviderMapper from './dataProviderMapper';
import TreeMenu from '@bb-tech/ra-treemenu';

import { SportList, SportEdit, SportCreate } from './sportList';
import { IncompleteList, IncompleteEdit } from './incompleteList';
import { QuestionList, QuestionEdit, QuestionCreate } from './questionList';
import { ScraperList } from './scraperList';
import { orderList } from './questionOrderList';
import { archiveList } from './archiveList';
import { criteriaList } from './criteriaList';
import { snackList, snackEdit, snackCreate } from './snackList';

import RestoreFromTrashIcon from '@material-ui/icons/RestoreFromTrash';
import SportsKabaddiIcon from '@material-ui/icons/SportsKabaddi';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import SportsFootballIcon from '@material-ui/icons/SportsFootball';
import ListAltIcon from '@material-ui/icons/ListAlt';
import YoutubeSearchedForIcon from '@material-ui/icons/YoutubeSearchedFor';
import FormatListNumberedIcon from '@material-ui/icons/FormatListNumbered';
import RestaurantIcon from '@material-ui/icons/Restaurant';
import AnnouncementIcon from '@material-ui/icons/Announcement';


const App = () => (
  <Admin
    dataProvider={dataProviderMapper} // A custom mapper is used because different resources need different dataProviders
    disableTelemetry
    menu={TreeMenu}
  >

    <Resource
      name='sportarten'
      options={{ "label": "Sportarten", "isMenuParent": true }}
    />

    <Resource
      name='sport'  // name of the API endpoint
      icon={SportsFootballIcon}
      options={{ label: 'Management', "menuParent": "sportarten" }} // if we do not rename the resource, the API name will be used
      list={SportList}
      edit={SportEdit}
      create={SportCreate}
    />

    <Resource
      name='sport-scraper'
      icon={YoutubeSearchedForIcon}
      options={{ label: 'Buchsys Auslesen', "menuParent": "sportarten" }}
      list={ScraperList}
    />

    <Resource
      name='sport-incomplete'
      icon={ListAltIcon}
      options={{ label: 'Fehlende Kriterien', "menuParent": "sportarten" }}
      list={IncompleteList}
      edit={IncompleteEdit}
    />

    <Resource
      name='sport-archive'
      icon={RestoreFromTrashIcon}
      options={{ label: 'Archiv', "menuParent": "sportarten" }}
      list={archiveList}
    />

    <Resource name='fragen' options={{ "label": "Fragen", "isMenuParent": true }} />

    <Resource
      name='question'
      icon={QuestionAnswerIcon}
      options={{ label: 'Management', "menuParent": "fragen" }}
      list={QuestionList}
      edit={QuestionEdit}
      create={QuestionCreate}
    />

    <Resource
      name='question-order'
      icon={FormatListNumberedIcon}
      options={{ label: 'Reihenfolge', "menuParent": "fragen" }}
      list={orderList}
    />

    <Resource
      name='criteria'
      icon={SportsKabaddiIcon}
      options={{ label: 'Kriterienübersicht', "menuParent": "fragen" }}
      list={criteriaList}
    />

    <Resource name='snacks_n_co' options={{ "label": "Snacks & Co.", "isMenuParent": true }} />

    <Resource
      name='snack'
      icon={RestaurantIcon}
      options={{ label: 'Wissenssnacks', "menuParent": "snacks_n_co" }}
      list={snackList}
      edit={snackEdit}
      create={snackCreate}
    />

    <Resource
      name='activity'
      icon={AnnouncementIcon}
      options={{ label: 'Aktivitäten', "menuParent": "snacks_n_co" }}
      list={snackList}
      edit={snackEdit}
      create={snackCreate}
    />


  </Admin>
);

export default App;
